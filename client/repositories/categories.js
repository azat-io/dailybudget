/**
 * Categories
 */
class CategoriesRepository {
  constructor () {
    this.baseUrl = 'http'
  }

  /**
   * Get all user's categories
   *
   * @returns {array} Categories list
   */
  all () {
  }
}

export default new CategoriesRepository()
